package CustomExceptions;

public class PerturbationException extends Exception {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -815545118198962467L;
	
	public PerturbationException(String message) {
		super(message);
	}
}
