package Helpers;

import java.util.List;

public class MathHelper {
	@SuppressWarnings("unchecked")
	public static <T extends Number> T getMin(List<T> list) throws Exception {
		if (!list.isEmpty()) {
			T first = list.get(0);
			if (first instanceof Float) {
				return (T) getMinFloat((List<Float>)list);
			} else if (first instanceof Long) {
				return (T) getMinLong((List<Long>)list);
			} else {
				throw new Exception("Unsupported list type");
			}
		}
		throw new Exception("Empty list");
	}

	@SuppressWarnings("unchecked")
	public static <T extends Number> T getMax(List<T> list) throws Exception {
		if (!list.isEmpty()) {
			T first = list.get(0);
			if (first instanceof Float) {
				return (T) getMaxFloat((List<Float>)list);
			} else if (first instanceof Long) {
				return (T) getMaxLong((List<Long>)list);
			} else {
				throw new Exception("Unsupported list type");
			}
		}
		throw new Exception("Empty list");
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Number> T getAvg(List<T> list) throws Exception {
		if (!list.isEmpty()) {
			T first = list.get(0);
			if (first instanceof Float) {
				return (T) getAvgFloat((List<Float>)list);
			} else if (first instanceof Long) {
				return (T) getAvgLong((List<Long>)list);
			} else {
				throw new Exception("Unsupported list type");
			}
		}
		throw new Exception("Empty list");
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Number> T getStdDev(List<T> list) throws Exception {
		if (!list.isEmpty()) {
			T first = list.get(0);
			if (first instanceof Float) {
				return (T) getStdDevFloat((List<Float>)list);
			} else if (first instanceof Long) {
				return (T) getStdDevLong((List<Long>)list);
			} else {
				throw new Exception("Unsupported list type");
			}
		}
		throw new Exception("Empty list");
	}

	private static Float getMinFloat(List<Float> list) {
		float min = Float.MAX_VALUE;	
		
		for (float cost : list) {
			if (min > cost) {
				min = cost;
			}
		}
		
		return min;
	}
	
	private static Long getMinLong(List<Long> list) {
		long min = Long.MAX_VALUE;	
		
		for (long e : list) {
			if (min > e) {
				min = e;
			}
		}
		
		return min;
	}
	
	private static Float getMaxFloat(List<Float> list) {
		float max = Float.MIN_VALUE;
		
		for (float e : list) {
			if (max < e) {
				max = e;
			}
		}
		
		return max;
	}
	
	private static Long getMaxLong(List<Long> list) {
		long max = Long.MIN_VALUE;
		
		for (long e : list) {
			if (max < e) {
				max = e;
			}
		}
		
		return max;
	}
	
	private static Float getAvgFloat(List<Float> list) {
		float sum = 0;
		
		for (float e : list) {
			sum += e;
		}
		
		return sum / list.size();
	}
	
	private static Long getAvgLong(List<Long> list) {
		long sum = 0;
		
		for (long e : list) {
			sum += e;
		}
		
		return sum / list.size();
	}
	
	private static Float getStdDevFloat(List<Float> list) {
		double sumstd = 0;
		float avg = getAvgFloat(list);
		
		for (float e : list) {
			sumstd += Math.pow(e - avg, 2);
		}
		
		return (float) Math.sqrt(sumstd / list.size());
	}
	
	private static Long getStdDevLong(List<Long> list) {
		double sumstd = 0;
		float avg = getAvgLong(list);
		
		for (long e : list) {
			sumstd += Math.pow(e - avg, 2);
		}
		
		return (long) Math.sqrt(sumstd / list.size());
	}
}
