package Main;

import java.io.File;

import Annealing.InitializationTypes;
import Annealing.PerturbationTypes;
import Annealing.SimulatedAnnealing;
import CoolingSchedule.CoolingScheduleTypes;
import CustomExceptions.PerturbationException;
import Helpers.FileSystemHelper;
import Sharing.Solution;
import Statistics.IStatisticsGrabber;
import Statistics.OwnStatisticsGrabber;
import Statistics.UruguayanStatisticsGrabber;

public class Main {
	public static void main(String[] args) throws Exception {
		
		// params
//		String costosDirectory = "costos/";
//		String outputDirectory = "output/";
		String salidasDirectory = "salidas/";
//		String statisticsDirectory = "statistics/";
		
		String costosDirectory = "tempCostos/";
		String outputDirectory = "tempOutput/";
		String statisticsDirectory = "tempStatistics/";
		int launchNumber = 10;
		float[][] saParams = {
//				{1000, 0.001f},
//				{10000, 0.01f},
//				{100000, 0.1f},
				{1000000, 1},
//				{10000000, 10}
		};
		InitializationTypes[] initTypes = {InitializationTypes.RANDOM};
		int[] loadnesses = {4}; 
		PerturbationTypes[] perturbationTypes = {PerturbationTypes.SWAP};
		String[] coolingScheduleTypes = {
//				CoolingScheduleTypes.HABRAHABR,
//				CoolingScheduleTypes.LOGARITHMICAL_MULTIPLICATIVE,
//				CoolingScheduleTypes.QUADRATIC_MULTIPLICATIVE,
//				CoolingScheduleTypes.EXPONENTIAL_ADDITIVE,
//				CoolingScheduleTypes.TRIGINOMETRIC_ADDITIVE,
//				CoolingScheduleTypes.NON_MONOTONIC_ADAPTIVE,
				CoolingScheduleTypes.EXPONENTIAL_MULTIPLICATIVE
		};
		
		// simulated annealing
		for (String cType : coolingScheduleTypes) {
			for (PerturbationTypes pType : perturbationTypes) {
				for (int loadness : loadnesses) {
					for (InitializationTypes iType : initTypes) {
						for (int j = 0; j < saParams.length; j++) {
							for (String filename : FileSystemHelper.listFilesForFolder(new File(costosDirectory))) {
								for (int i = 0; i < launchNumber; i++) {
									Solution s = SimulatedAnnealing.simulatedAnnealing(
											costosDirectory + filename,
											saParams[j][0], saParams[j][1],
											iType, loadness, pType, cType);
									String outputDirName = outputDirectory + filename + "/";
									FileSystemHelper.createFolder(new File(outputDirName));
									s.saveSolution(outputDirName + i + ".txt");
								}
							}
						
						// statistics
							String outputDirName = statisticsDirectory + cType + "/";
							FileSystemHelper.createFolder(new File(outputDirName));
							IStatisticsGrabber osGrabber = new OwnStatisticsGrabber(outputDirectory, outputDirName);
							osGrabber.gatherStatistics();
						}
					}
				}
			}
		}
//		String outputDirName = statisticsDirectory + "uruguayan/";
//		FileSystemHelper.createFolder(new File(outputDirName));
//		IStatisticsGrabber usGrabber = new UruguayanStatisticsGrabber(salidasDirectory, outputDirName);
//		usGrabber.gatherStatistics();
//		
//		Solution s = SimulatedAnnealing.simulatedAnnealing(costosDirectory + "s1.txt", 1000000, 1, InitializationTypes.RANDOM, 4, PerturbationTypes.SWAP,
//				CoolingScheduleTypes.NON_MONOTONIC_ADAPTIVE);
//		System.out.println("time: " + s.getTime());
	}
}
