package CoolingSchedule;

public abstract class BaseCoolingSchedule implements ICoolingSchedule {

	protected double initialTemperature;
	protected double finalTemperature;
	
	@Override
	public void prepare(double initialTemperature, double finalTemperature) {
		this.initialTemperature = initialTemperature;
		this.finalTemperature = finalTemperature;
	}
}
