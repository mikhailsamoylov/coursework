package CoolingSchedule;

public class ExponentialAdditiveCoolingSchedule extends BaseCoolingSchedule {

	private int n = 42927; // 100001 iterations
	
	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		return finalTemperature + (initialTemperature - finalTemperature) *
				(1 / (1 + Math.exp((2 * Math.log(initialTemperature - finalTemperature) / n) * (currentIteration - n / 2))));
	}

}
