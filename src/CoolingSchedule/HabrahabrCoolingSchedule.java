package CoolingSchedule;

public class HabrahabrCoolingSchedule extends BaseCoolingSchedule {
	
	private final double multiplicator = 0.1; // 100000 iterations
	
	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		 return initialTemperature * multiplicator / currentIteration;
	}
}
