package CoolingSchedule;

public class LogarithmicalMultiplicativeCoolingSchedule extends BaseCoolingSchedule {
	
	/** Should be more than 1 */
	private final int alpha = 86858; // 100010 iterations

	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		return initialTemperature / (1 + alpha * Math.log(1 + currentIteration));
	}

}
