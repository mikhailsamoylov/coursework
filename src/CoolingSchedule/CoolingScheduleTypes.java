package CoolingSchedule;

public class CoolingScheduleTypes {
	public static final String HABRAHABR = "HabrahabrCoolingSchedule";
	public static final String LOGARITHMICAL_MULTIPLICATIVE = "LogarithmicalMultiplicativeCoolingSchedule";
	public static final String QUADRATIC_MULTIPLICATIVE = "QuadraticMultiplicativeCoolingSchedule";
	public static final String EXPONENTIAL_ADDITIVE = "ExponentialAdditiveCoolingSchedule";
	public static final String TRIGINOMETRIC_ADDITIVE = "TrigonometricAdditiveCoolingSchedule";
	public static final String NON_MONOTONIC_ADAPTIVE = "NonMonotonicAdaptiveCoolingSchedule";
	public static final String EXPONENTIAL_MULTIPLICATIVE = "ExponentialMultiplicativeCoolingSchedule";
}
