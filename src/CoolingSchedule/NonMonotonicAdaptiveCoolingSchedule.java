package CoolingSchedule;

public class NonMonotonicAdaptiveCoolingSchedule extends BaseCoolingSchedule {

	private double alpha = 0.0008;
	
	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		return currentTemperature * (1 + alpha * ((bestEnergy - currentEnergy) / currentEnergy));
	}

}
