package CoolingSchedule;

public class TrigonometricAdditiveCoolingSchedule extends BaseCoolingSchedule {

	private int n = 100000; // 100000 iterations
	
	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		return finalTemperature + (initialTemperature - finalTemperature) / 2 *
				(1 + Math.cos(currentIteration * Math.PI / n));
	}

}
