package CoolingSchedule;

public class ExponentialMultiplicativeCoolingSchedule extends BaseCoolingSchedule {

	private final double alpha = 0.999861854; // 100000 iterations
	
	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		return initialTemperature * Math.pow(alpha, currentIteration);
	}

}
