package CoolingSchedule;

public class QuadraticMultiplicativeCoolingSchedule extends BaseCoolingSchedule {

	/** Should be more than 0 */
	private final double alpha = 0.0001; // 100000 iterations
	
	@Override
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy,
			double bestEnergy) {
		return initialTemperature / (1 + alpha * Math.pow(currentIteration, 2));
	}

}
