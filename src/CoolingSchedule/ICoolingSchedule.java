package CoolingSchedule;

public interface ICoolingSchedule {
	public void prepare(double initialTemperature, double finalTemperature);
	public double decreaseTemperature(double currentTemperature, int currentIteration, double currentEnergy, double bestEnergy);
}
