package Annealing;

import java.lang.reflect.Constructor;

import CoolingSchedule.*;
import Sharing.Solution;
import Sharing.Tour;

public class SimulatedAnnealing {
	private static Tour best;
	private static ICoolingSchedule schedule;
	
	public static Solution simulatedAnnealing(String filename) {
		return simulatedAnnealing(filename, 1000000, 1, InitializationTypes.RANDOM, 4, PerturbationTypes.SWAP, CoolingScheduleTypes.HABRAHABR);
	}
	
	public static Solution simulatedAnnealing(String filename, double initialTemperature, double minimalTemperature, InitializationTypes initType, int loadness,
			PerturbationTypes perturbationType, String coolingScheduleType) {

		try {
			setScheduleType(coolingScheduleType);
			schedule.prepare(initialTemperature, minimalTemperature);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long startTime = System.currentTimeMillis();
        // Initialize initial solution
        Tour currentSolution = new Tour(filename);
        currentSolution.setLoadness(loadness);
        
        try {
        	currentSolution.init(initType);
		} catch (Exception e) {
			e.getMessage();
		}
     
        double temp = initialTemperature;

        // Set as current best
        best = new Tour(currentSolution);
        float bestEnergy = best.getDistance();
        float previousBestEnergy = bestEnergy;

        int iter = 1;

        // Loop until system has cooled
        while (temp > minimalTemperature) {
            // Create new neighbour tour
            Tour newSolution = new Tour(currentSolution);

            // Tour perturbation
			try {
				newSolution.perturbate(perturbationType);
			} catch (Exception e) {
				e.getMessage();
			}

            // Get energy of solutions
			float currentEnergy = currentSolution.getDistance();
            float neighbourEnergy = newSolution.getDistance();

            // Decide if we should accept the neighbour
            if (acceptanceProbability(currentEnergy, neighbourEnergy, temp) > Math.random()) {
                currentSolution = new Tour(newSolution);
            }

            // Keep track of the best solution found
			if (currentSolution.getDistance() < best.getDistance()) {
			    best = new Tour(currentSolution);
			    previousBestEnergy = bestEnergy;
			    bestEnergy = best.getDistance();
				System.out.println("\rBest cost: " + bestEnergy + ", temp: " + temp);
			}

            // Cool system
            temp = schedule.decreaseTemperature(temp, iter, neighbourEnergy, previousBestEnergy);
            iter++;
        }
        long endTime = System.currentTimeMillis();

        System.out.println("\rBest cost: " + bestEnergy);
        best.printTour();

        System.out.println("\riter: " + iter);
        
        return new Solution(bestEnergy, best.getTour(), endTime - startTime);
    }
	
	private static double acceptanceProbability(float energy, float newEnergy, double temperature) {
        // If the new solution is better, accept it
        if (newEnergy < energy) {
            return 1.0;
        }
        // If the new solution is worse, calculate an acceptance probability
        return Math.exp((energy - newEnergy) / temperature);
    }
	
	private static void setScheduleType(String schedule) throws Exception {
		Class<?> scheduleType = Class.forName("CoolingSchedule." + schedule);
		Constructor<?> ctor = scheduleType.getConstructor();
		SimulatedAnnealing.schedule = (ICoolingSchedule) ctor.newInstance();
	}
}
