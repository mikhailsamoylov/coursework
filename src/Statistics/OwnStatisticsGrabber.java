package Statistics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import Sharing.Parser;
import Sharing.Solution;

public class OwnStatisticsGrabber extends BaseStatisticsGrabber {
	
	public OwnStatisticsGrabber(String solutionDirectory, String statisticsDirectory) {
		super(solutionDirectory, statisticsDirectory);
	}

	public Solution parseData(String filename) {
		String cost = null;
		String time = null;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			cost = br.readLine();
			br.readLine();
			time = br.readLine();
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new Solution(Float.parseFloat(cost), Parser.getTaxiMatrix(filename, 1), Long.parseLong(time));
	}
}
