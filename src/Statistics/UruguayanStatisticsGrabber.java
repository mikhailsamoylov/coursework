package Statistics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Sharing.Matrix;
import Sharing.Parser;
import Sharing.Solution;

public class UruguayanStatisticsGrabber extends BaseStatisticsGrabber {

	public UruguayanStatisticsGrabber(String solutionDirectory, String statisticsDirectory) {
		super(solutionDirectory, statisticsDirectory);
	}

	@Override
	protected Solution parseData(String filename) {		
		float cost = 0;
		Matrix<Integer> tMatrix = null;
		long time = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String regexp = "Time spent in trial: ([\\d]\\.[\\d]+e\\+[\\d]+)";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher;
			
			String temp = br.readLine();
			boolean timeWasFound = false;
			while (temp != null) {
				matcher = pattern.matcher(temp);
				if (!timeWasFound && matcher.find()) {
					time = Double.valueOf(matcher.group(1)).longValue() / 1000;
					timeWasFound = true;
					temp = br.readLine();
					continue;
				}
				
				if (timeWasFound) {
					regexp = "Solution:  ((?:[\\d]+ )+)";
					pattern = Pattern.compile(regexp);
					matcher = pattern.matcher(temp);
					if (matcher.find()) {
						tMatrix = Parser.getTaxiMatrixByLine(matcher.group(1));
						regexp = "Fitness: ([\\d]+\\.[\\d]+|[\\d]+)";
						pattern = Pattern.compile(regexp);
						matcher = pattern.matcher(temp);
						if (matcher.find()) {
							cost = Float.parseFloat(matcher.group(1));
							break;
						}
					}
				}
				temp = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new Solution(cost, tMatrix, time);
	}

}
