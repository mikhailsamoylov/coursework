package Statistics;

public class Statistics {
	private float minCost;
	private float maxCost;
	private float avgCost;
	private float stddevCost;
	
	private long minTime;
	private long maxTime;
	private long avgTime;
	private long stddevTime;

	public Statistics (float min, float max, float avg, float stddev,
			long minTime, long maxTime, long avgTime, long stddevTime
	) {
		this.minCost = min;
		this.maxCost = max;
		this.avgCost = avg;
		this.stddevCost = stddev;
		
		this.minTime = minTime;
		this.maxTime = maxTime;
		this.avgTime = avgTime;
		this.stddevTime = stddevTime;
	}

	public float getMinCost() {
		return minCost;
	}

	public float getMaxCost() {
		return maxCost;
	}

	public double getAvgCost() {
		return avgCost;
	}

	public double getStddevCost() {
		return stddevCost;
	}
	
	public long getMinTime() {
		return minTime;
	}

	public long getMaxTime() {
		return maxTime;
	}

	public double getAvgTime() {
		return avgTime;
	}

	public double getStddevTime() {
		return stddevTime;
	}
}
