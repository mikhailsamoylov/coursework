package Statistics;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import Helpers.FileSystemHelper;
import Sharing.Solution;

import static Helpers.MathHelper.*;

abstract public class BaseStatisticsGrabber implements IStatisticsGrabber {
	
	private String solutionDirectory;
	private String statisticsDirectory;
	
	public BaseStatisticsGrabber (String solutionDirectory, String statisticsDirectory) {
		this.solutionDirectory = solutionDirectory;
		this.statisticsDirectory = statisticsDirectory;
	}
	
	public void gatherStatistics() {
		Map<String, List<String>> solutionFiles = FileSystemHelper.listFilesForFolder2(new File(solutionDirectory));
		for (String folder : solutionFiles.keySet()) {
			List<Float> bestCosts = new ArrayList<Float>();
			List<Long> times = new ArrayList<Long>();
			for (String solutionFilename : solutionFiles.get(folder)) {
				Solution s = parseData(solutionDirectory + folder + "/" + solutionFilename);
				bestCosts.add(s.getCost());
				times.add(s.getTime());
			}
			Statistics stat = calculate(bestCosts, times);
			try {
				save(statisticsDirectory + folder + ".csv", stat);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void save(String filename, Statistics stat) throws IOException {
		PrintWriter writer = null;
		String separator = ";";
		try {
		    writer = new PrintWriter(filename, "UTF-8");
		    
		    writer.print(stat.getMinCost() + separator);
		    writer.print(stat.getMaxCost() + separator);
		    writer.print(stat.getAvgCost() + separator);
		    writer.println(stat.getStddevCost());
		    
		    writer.print(stat.getMinTime() + separator);
		    writer.print(stat.getMaxTime() + separator);
		    writer.print(stat.getAvgTime() + separator);
		    writer.println(stat.getStddevTime());
		} catch (IOException e) {
			throw e;
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	protected Statistics calculate(List<Float> bestCosts, List<Long> times) {
		try {
			return new Statistics(getMin(bestCosts), getMax(bestCosts), getAvg(bestCosts), getStdDev(bestCosts),
					getMin(times), getMax(times), getAvg(times), getStdDev(times));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	abstract protected Solution parseData(String filename);
}
