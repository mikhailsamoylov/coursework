package Sharing;

import java.io.IOException;
import java.io.PrintWriter;

public class Solution {
	private float cost;
	private Matrix<Integer> tour;
	private long time;
	
	public Solution (float cost, Matrix<Integer> tour, long time) {
		this.cost = cost;
		this.tour = tour;
		this.time = time;
	}
	
	public float getCost() {
		return cost;
	}
	
	public Matrix<Integer> getTour() {
		return tour;
	}
	
	public long getTime() {
		return time;
	}
	
	public void saveSolution(String filename) throws Exception {
		PrintWriter writer = null;
		try {
		    writer = new PrintWriter(filename, "UTF-8");
		    writer.println(cost);

		    for (int i = tour.getMinRowIndex(); i <= tour.getMaxRowIndex(); i++) {
				for (int j = tour.getMinColumnIndex(i); j <= tour.getMaxColumnIndex(i); j++) {
					try {
						writer.print(tour.getValueByCoordinate(i,  j) + " ");
					} catch (Exception e) {
						throw e;
					}
				}
				writer.print("0 ");
			}
		    writer.println();
		    writer.println(time);
		    
		} catch (IOException e) {
			throw e;
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
}
