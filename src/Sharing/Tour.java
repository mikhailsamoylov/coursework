package Sharing;

import java.util.ArrayList;
import java.util.List;

import Annealing.InitializationTypes;
import Annealing.PerturbationTypes;
import CustomExceptions.PerturbationException;

public class Tour {
	private Matrix<Integer> tour;
	private Matrix<Float> costs;
 	private int capacity = 4;
 	private int loadness = 4;
 	private int destinationCount;
	
	public Tour(String filename) {
		tour = new Matrix<Integer>();
		costs = Parser.getCostMatrix(filename);
		destinationCount = costs.getMaxRowIndex() + 1;
	}
	
	public Tour(String filename, int capacity) {
		this(filename);
		this.capacity = capacity;
	}

	public Tour(Tour tour) {
		this.tour = (Matrix<Integer>) tour.tour.clone();
		this.costs = (Matrix<Float>) tour.costs.clone();
		this.capacity = tour.capacity;
		this.loadness = tour.loadness;
		this.destinationCount = tour.destinationCount;
	}
	
	public void setLoadness(int loadness) {
		this.loadness = loadness;
	}

	private void stupidInit() throws Exception {
		if (loadness > capacity) {
			throw new Exception("Impossible loadness");
		}
		int rowPointer = 0, columnPointer = 0;
		int i = 1;
		while (i < destinationCount) {
			if (columnPointer >= loadness) {
				rowPointer++;
				columnPointer = 0;
			}
			tour.setCoordinate(rowPointer, columnPointer++, i++);
		}
	}
	
	private void randomInit() throws Exception {
		if (loadness > capacity) {
			throw new Exception("Impossible loadness");
		}
		List<Integer> passengers = new ArrayList<Integer>();
		for (int i = 1; i < destinationCount; i++) {
			passengers.add(i);
		}
		
		int rowPointer = 0, columnPointer = 0;
		while (!passengers.isEmpty()) {
			if (columnPointer >= loadness) {
				rowPointer++;
				columnPointer = 0;
			}
			tour.setCoordinate(rowPointer, columnPointer++,
					passengers.remove((int) (Math.random() * passengers.size())));
		}
	}
	
	private void greedyInit() throws Exception {
		if (loadness > capacity) {
			throw new Exception("Impossible loadness");
		}
		
		boolean[] passengersMapping = new boolean[destinationCount];
		for (int i = 1; i < destinationCount; i++) {
			passengersMapping[i] = true;
		}
		
		int rowPointer = 0, columnPointer = 0, currentPassengerIndex = 0;
		for (int i = 1; i < destinationCount; i++) {
			if (columnPointer >= loadness) {
				rowPointer++;
				columnPointer = 0;
				currentPassengerIndex = 0;
			}
			
			currentPassengerIndex = getClosestPassengerIndex(currentPassengerIndex, passengersMapping);			
			tour.setCoordinate(rowPointer, columnPointer++, currentPassengerIndex);
			passengersMapping[currentPassengerIndex] = false;
		}
	}
	
	private int getClosestPassengerIndex(int origin, boolean[] passengersMapping) {
		float currentBestCost = Float.MAX_VALUE;
		int currentBestIndex = 0;
		for (int i = 1; i < destinationCount; i++) {
			if (!passengersMapping[i]) {
				continue;
			}
			float candidate = costs.getValueByCoordinate(origin, i);
			if (candidate < currentBestCost) {
				currentBestCost = candidate;
				currentBestIndex = i;
			}
		}
		return currentBestIndex;
	}

	public void printTour() {
		tour.printMe();
	}
	
	public Matrix<Integer> getTour() {
		return tour;
	}

	
	public void swapRandomPassengers() {
		Coordinate c1 = tour.getRandomCoordinate();
		Coordinate c2 = tour.getRandomCoordinate();
		swapPassengers(c1, c2);
	}

	private void swapPassengers(Coordinate c1, Coordinate c2) {
		int tempPassenger = tour.getValueByCoordinate(c1);
		tour.setCoordinate(c1, tour.getValueByCoordinate(c2));
		tour.setCoordinate(c2, tempPassenger);
	}

	public float getDistance() {
		float fdist = 0;
		float fTotalCost = 0;
		for (int j = 0; j <= tour.getMaxRowIndex(); j++) {
			try {
				fdist = costs.getValueByCoordinate(tour.getValueByCoordinate(j, 0), 0);
			} catch (Exception e) {
				e.getMessage();
			}
			for (int i = 1; i <= tour.getMaxColumnIndex(j); i++) {
				fdist += costs.getValueByCoordinate(tour.getValueByCoordinate(j, i), tour.getValueByCoordinate(j, i - 1));
			}
			fTotalCost += (13 + fdist);
			fdist = 0;
		}
		return fTotalCost;
	}
	
	public int getDestinationCount() {
		return destinationCount;
	}

	public void init(InitializationTypes type) throws Exception {
		switch (type) {
			case STUPID:
				stupidInit();
				break;
			case RANDOM:
				randomInit();
				break;
			case GREEDY:
				greedyInit();
				break;
			default:
				throw new Exception("Invalid initialization type");
		}
	}
	
	public void perturbate(PerturbationTypes type) throws Exception {
		switch (type) {
			case SWAP:
				swapRandomPassengers();
				break;
			case DELETE_AND_INSERT:
				deleteAndInsert();
				break;
			default:
				throw new Exception("Invalid perturbation type");
		}
	}

	public void deleteAndInsert() {
		int taxiIndex = -1;
		try {
			taxiIndex = getRandomNotFullTaxiIndex();
		} catch (PerturbationException e) {
			System.out.println("Can not perturbate deleteAndInsert() method, swapRandomPassengers() is using instead");
			swapRandomPassengers();
			return;
		}
		
		Coordinate passengerToMove = null;
		boolean result = false;
		int passenger = -1;
		while (!result) {
			passengerToMove = tour.getRandomCoordinate();
			passenger = tour.getValueByCoordinate(passengerToMove);
			result = deletePassenger(passengerToMove);
		}
		
		pastePassenger(taxiIndex, passenger);
	}

	private void pastePassenger(int taxiIndex, int passenger) {
		int lastRow = tour.getMaxColumnIndex(taxiIndex);
		tour.setCoordinate(new Coordinate(taxiIndex, lastRow + 1), passenger);
	}

	private int getRandomNotFullTaxiIndex() throws PerturbationException {
		List<Integer> notFullTaxiIndices = getNotFullTaxiIndices();
		if (notFullTaxiIndices.isEmpty()) {
			throw new PerturbationException("Can not perturbate like this");
		}
		int index = notFullTaxiIndices.get((int) (Math.random() * notFullTaxiIndices.size()));
		return index;
	}

	private List<Integer> getNotFullTaxiIndices() {
		List<Integer> indices = new ArrayList<Integer>();
		int startPos = tour.getMinRowIndex();
		int finalPos = tour.getMaxRowIndex();
		for (int i = startPos; i <= finalPos; i++) {
			if (tour.getMaxColumnIndex(i) + 1 < loadness) {
				indices.add(i);
			}
		}
		return indices;
	}

	private boolean deletePassenger(Coordinate passengerToMove) {
		int rowIndex = passengerToMove.getRowIndex();
		int startPos = passengerToMove.getColumnIndex();
		int finalPos = tour.getMaxColumnIndex(rowIndex);
		if (finalPos == 0) {
			return false;
		}
		for (int i = startPos; i < finalPos; i++) {
			tour.setCoordinate(new Coordinate(rowIndex, i), tour.getValueByCoordinate(new Coordinate(rowIndex, i + 1)));
		}
		tour.deleteCoordinate(new Coordinate(rowIndex, finalPos));
		return true;
	}
}
