package Sharing;

public interface IMatrix<T> {
	public void setCoordinate(int row, int column, T value);
	public T getValueByCoordinate(int row, int column) throws Exception;
}
