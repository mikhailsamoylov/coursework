package Sharing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
	public static Matrix<Integer> getDuracionMatrix(String filename) {
		Matrix<Integer> matrix = new Matrix<Integer>();
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String regexp = "([\\d]+)";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher;
			
			String temp = br.readLine();
			int rowPointer = 0, columnPointer = 0;
			
			while (temp != null) {
				matcher = pattern.matcher(temp);
				while (matcher.find()) {
					matrix.setCoordinate(rowPointer, columnPointer++, Integer.parseInt(matcher.group(1)));
				}
				rowPointer++;
				columnPointer = 0;
				temp = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return matrix;
	}
	
	public static Matrix<Float> getCostMatrix(String filename) {
		Matrix<Float> matrix = new Matrix<Float>();
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String regexp = "([\\d]+\\.[\\d]+|[\\d]+)";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher;
			
			int rowPointer = 0, columnPointer = 0;
			
			for (int i = 0; i < 5; i++) {
				br.readLine();
			}
			
			String temp = br.readLine();
			while (temp != null) {
				matcher = pattern.matcher(temp);
				while (matcher.find()) {
					matrix.setCoordinate(rowPointer, columnPointer++, Float.parseFloat(matcher.group(1)));
				}
				rowPointer++;
				columnPointer = 0;
				temp = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return matrix;
	}
	
	public static Matrix<Integer> getTaxiMatrixByLine(String line) {
		Matrix<Integer> matrix = new Matrix<Integer>();
		String regexp = "([\\d]+\\.[\\d]+|[\\d]+)";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(line);
		
		int rowPointer = 0, columnPointer = 0;
		while (matcher.find()) {
			int passenger = Integer.parseInt(matcher.group(1));
			if (passenger == 0) {
				rowPointer++;
				columnPointer = 0;
			} else {
				matrix.setCoordinate(rowPointer, columnPointer++, passenger);
			}
		}
		return matrix;
	}

	public static Matrix<Integer> getTaxiMatrix(String filename) {
		return getTaxiMatrix(filename, 4);
	}
	
	public static Matrix<Integer> getTaxiMatrix(String filename, int line) {		
		Matrix<Integer> matrix = new Matrix<Integer>();
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			
			for (int i = 0; i < line; i++) {
				br.readLine();
			}
			
			String temp = br.readLine();
			
			matrix = getTaxiMatrixByLine(temp);
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return matrix;
	}
}
