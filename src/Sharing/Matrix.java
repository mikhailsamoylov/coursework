package Sharing;

import java.util.HashMap;

import Sharing.Coordinate;

public class Matrix<T> implements IMatrix<T>, Cloneable {
	private HashMap<Coordinate, T> matrix;
	private int maxRow;
	private int minRow = Integer.MAX_VALUE;
	private HashMap<Integer, Integer> maxColumns;
	private HashMap<Integer, Integer> minColumns;
	
	public Matrix() {
		matrix = new HashMap<Coordinate, T>();
		maxColumns = new HashMap<Integer, Integer>();
		minColumns = new HashMap<Integer, Integer>();
	}
	
	@SuppressWarnings("unchecked")
	public Matrix<T> clone() {
        Matrix<T> clonedMatrix = new Matrix<T>();
        
        clonedMatrix.matrix = (HashMap<Coordinate, T>) this.matrix.clone();
        clonedMatrix.maxColumns = (HashMap<Integer, Integer>) this.maxColumns.clone();
        clonedMatrix.minColumns = (HashMap<Integer, Integer>) this.minColumns.clone();
        clonedMatrix.maxRow = this.maxRow;
        clonedMatrix.minRow = this.minRow;
        
        return clonedMatrix;
    }
	
	public T getValueByCoordinate(int row, int column) {
		return getValueByCoordinate(new Coordinate(row, column));
	}
	
	public T getValueByCoordinate(Coordinate coordinate) {
		try {
			T value = matrix.get(coordinate);
			if (value == null) {
				throw new Exception("Out of bounds, man");
			}
			return value;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	public void setCoordinate(int row, int column, T value) {
		setCoordinate(new Coordinate(row, column), value);
	}
	
	public void setCoordinate(Coordinate c, T value) {
		matrix.put(c, value);
		setBounds(c.getRowIndex(), c.getColumnIndex()); // TODO: bounds should be calculated when they are needed
	}
	
	private void setBounds(int rowIndex, int columnIndex) {
		// set max row index
		if (rowIndex > maxRow) {
			maxRow = rowIndex;
		}
		
		// set max column index for row
		Integer currentMaxRow = maxColumns.get(rowIndex);
		if (currentMaxRow == null || columnIndex > currentMaxRow) {
			maxColumns.put(rowIndex, columnIndex);
		}
		
		// set min row index
		if (rowIndex < minRow) {
			minRow = rowIndex;
		}
		
		// set min column index for row
		Integer currentMinRow = minColumns.get(rowIndex);
		if (currentMinRow == null || columnIndex < currentMinRow) {
			minColumns.put(rowIndex, columnIndex);
		}
	}
	
	public int getMaxRowIndex() {
		return maxRow;
	}
	
	public int getMaxColumnIndex(int rowIndex) {
		return maxColumns.get(rowIndex);
	}
	
	public int getMinRowIndex() {
		return minRow;
	}
	
	public int getMinColumnIndex(int rowIndex) {
		return minColumns.get(rowIndex);
	}
	
	public void printMe() {
		for (int i = getMinRowIndex(); i <= getMaxRowIndex(); i++) {
			for (int j = getMinColumnIndex(i); j <= getMaxColumnIndex(i); j++) {
				try {
					System.out.print(getValueByCoordinate(i,  j) + " ");
				} catch (Exception e) {
					e.getMessage();
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public Coordinate getRandomCoordinate() {
		int size = matrix.size();
		int randomNumber = (int) (Math.random() * size);
		
		int currentPos = 0, x = 0, y = 0;
		do {
			int maxColumnIndex = getMaxColumnIndex(x);
			if (currentPos + maxColumnIndex >= randomNumber) {
				y = randomNumber - currentPos;
			} else {
				x++;
			}
			currentPos += maxColumnIndex + 1;
		} while (currentPos <= randomNumber);
		
		return new Coordinate(x, y);
	}
	
	public int getElementNumbers() {
		int count = 0;
		for (int i = minRow; i <= maxRow; i++) {
			count += getMaxColumnIndex(i);
		}
		return count;
	}

	public void deleteCoordinate(Coordinate coordinate) {
		int rowIndex = coordinate.getRowIndex();
		int maxIndex = getMaxColumnIndex(rowIndex);
		maxColumns.put(rowIndex, maxIndex - 1);
		matrix.remove(coordinate);
	}
}
